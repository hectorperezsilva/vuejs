import Vue from 'vue'
import vueRouter from 'vue-router'
import home from './home.vue'
import login from './login.vue'
import dashboard from './dashboard.vue'
import users from './users.vue'
import products from './products.vue'
import charts from './charts.vue'
import newUser from './newUser'
import update from './update.vue'

Vue.use(vueRouter)

const routes = [
    {path: '/', component: home, children: [
      {path: '/',component: dashboard},
      {path: '/users', component: users},
      {path: '/users/new', component: newUser},
      {path: '/users/editar/:id', component: update},
      {path: '/products', component: products},
      {path: '/charts', component: charts}
    ]},
    {path: '/login', component: login}
  ]
  
  const router = new vueRouter({
    routes,
    mode: 'history'
  })

  export default router