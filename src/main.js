import Vue from 'vue'
import Axios from 'axios'
import vueRouter from 'vue-router'
import App from './App.vue'
import router from './router'

window.Axios = Axios

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})


